import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';
import { DescriptionsItemProps } from 'antd/lib/descriptions/Item';

const PersonDetail = () => {
  const router = useRouter();

  const [isEditing, setEdit] = useState(false);

  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [updatedData, setUpdatedData] = useState<ReturnType<typeof usePersonInformation>['data']>(data);

  const handleUpdate = <K extends keyof typeof updatedData>(key: K, value: typeof updatedData[K] ) =>
    setUpdatedData(prevData => ({...prevData, [key]: value }))

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setEdit(true)}>
            Edit
          </Button>,
          <Button type="primary" onClick={() => {
            save(updatedData)
            setEdit(false);
          }}>
            Save
          </Button>
        ]}
      >
        {data && (
          <EditableItems
            isEditing={isEditing}
            handleEdit={handleUpdate}
            data={[
              { label: 'Name', content: updatedData.name },
              { label: 'Gender', content: updatedData.gender },
              { label: 'Phone', content: updatedData.phone },
              { label: 'Website', content: updatedData.website },
            ]}
            wrapper={(child) => isEditing ? <>{child}</> : <Descriptions size="small" column={1}>{child}</Descriptions>}
          />
        )}
        <GenericList
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

const EditableItems = ({
  isEditing = false,
  data,
  handleEdit,
  wrapper
}: {
  handleEdit: any;
  wrapper: (child) => JSX.Element;
  isEditing: boolean;
  data: { content: string; label: string }[];
}) => {
  const content = 
  <>
    {data.map(({ content, label }) =>
      isEditing ? (
        <Input value={content} onChange={({ target: { value } }) => handleEdit(label.toLowerCase(), value)} key={label} />
      ) : (
        <Descriptions.Item key={label} label={label}>
          {content}
        </Descriptions.Item>
      )
    )}
  </>;

  return wrapper(content)
};

export default withContextInitialized(PersonDetail);
